<head>
    <meta charset="utf-8">
    <title>FormWizard_v10</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="colorlib.com">

    <!-- MATERIAL DESIGN ICONIC FONT -->
    <link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.css">

    <!-- DATE-PICKER -->
    <link rel="stylesheet" href="vendor/date-picker/css/datepicker.min.css">

    <!-- STYLE CSS -->
    <link rel="stylesheet" href="css/style.css">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


</head>
<nav class="navbar navbar-expand-lg navbar-light  bg-secondary">
    <a class="navbar-brand" href="#">TEST hehe</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link active" href="http://localhost:80/">Home</a>
            <a class="nav-item nav-link" href="../dynamics_tables/dynamics_tables.php">Form</a>
            <a class="nav-item nav-link" href="../messages.php">Mes messages</a>

            </div>
        </div>
    </div>
</nav>


<div class="wrapper">
    <form action="../functions/traitementstest.php?" method="post">
        <!-- SECTION 1 -->

        <section>

            <div class="inner">
                <h4>New User</h4>
                <a href="#" class="avartar">
                    <img src="images/avartar.png" alt="">
                </a>

                <div class="form-row form-group">
                    <div class="form-holder">
                        <input type="text" class="form-control" name="name" placeholder="Name">
                    </div>

                </div>

                <div class="form-row">
                    <div class="form-holder">
                        <input type="text" class="form-control" name="adresse" placeholder="Adresse">
                        <i class="zmdi zmdi-map small"></i>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-holder">
                        <input type="number" class="form-control" name="age" placeholder="Age">

                    </div>
                </div>


                <!--Model Popup starts-->
                <div class="container">
                    <div class="row">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit" data-toggle="modal" href="#ignismyModal">





                    </div>

                    <?php if (isset($_GET["value"]) == "registered") {
                        
                    ?>

                        <div class="modal-body">

                            <div class="thank-you-pop">
                                <img src="http://goactionstations.co.uk/wp-content/uploads/2017/03/Green-Round-Tick.png" alt="">
                                <h1>Vous vous êtes enregistré !</h1>
                            </div>

                        </div>
                    <?php
                    } ?>

                </div>
                <!--Model Popup ends-->
            </div>



        </section>


    </form>
    <?php
    function dbConnect()
    {
        $host = 'db'; //Nom donné dans le docker-compose.yml
        $user = 'myuser'; // user et pwd du docker compose
        $password = 'mypassword';
        $db = 'tutoseu';

        $conn = new mysqli($host, $user, $password, $db);

        if (!$conn) {
            return false;
        } else {
            return $conn;
        }
    }

    ?>
    <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example">



        <?php
        if (dbConnect()) {
            $db = dbConnect();

            $tables = "SHOW TABLES";
            $query = $db->query($tables);

            while ($results = $query->fetch_assoc()) {

                foreach ($results as $key => $result) {

        ?>
                    <pre>
                    <option value="<?php $result ?>"><?= $result ?></option> ?>
                <pre>

        <?php
                }
            }
        };
        ?>
    </select>
            
</div>
<footer class=" bg-dark p-1">
      <div class="container p-3">
          <div class="footer-wrapper">

              <div class="col-lg-4 col-md-6">


                  <p class="small text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                      tempor incididunt.</p>
                  <p class="small text-muted mb-0">&copy;Ssonyan</p>
              </div>


          </div>
      </div>
  </footer>

        