<style>
    td {
        border: 1px solid black;
        padding: 4px;
    }
</style>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>




    <!-- MATERIAL DESIGN ICONIC FONT -->
    <link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.css">

    <!-- DATE-PICKER -->
    <link rel="stylesheet" href="vendor/date-picker/css/datepicker.min.css">

    <!-- STYLE CSS -->
    <link rel="stylesheet" href="css/style.css">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light  bg-secondary">
        <a class="navbar-brand" href="#">TEST hehe</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link active" href="http://localhost:80/">Home</a>
                <a class="nav-item nav-link" href="../dynamics_tables/dynamics_tables.php">Form</a>
                <a class="nav-item nav-link" href="messages.php">Mes messages</a>

            </div>
        </div>
        </div>
    </nav>



    <main>
        <?php
        function dbConnect()
        {
            $host = 'db'; //Nom donné dans le docker-compose.yml
            $user = 'myuser'; // user et pwd du docker compose
            $password = 'mypassword';
            $db = 'tutoseu';

            $conn = new mysqli($host, $user, $password, $db);

            if (!$conn) {
                return false;
            } else {
                return $conn;
            }
        }

        if (dbConnect()) {
            $db = dbConnect();


            $activities = "SELECT * FROM `Activitie`";
            $query = $db->query($activities);

            echo "<table>
          <tr>
            <th>ID Activités</th>
            <th>Nom</th>
            <th>Coût</th>
            <th>Prix</th>
          </tr>";
            while ($results = $query->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $results['idActivitie'] . "</td>";

                echo "<td>" . $results['name'] . "</td>";

                echo "<td>" . $results['cout'] . "</td>";

                echo "<td>" . $results['prix'] . "</td>";

                echo "</tr>";
            };
            $db->close();
        }
        ?>
    </main>


    <footer class=" bg-dark p-1">
        <div class="container p-3">
            <div class="footer-wrapper">

                <div class="col-lg-4 col-md-6">


                    <p class="small text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt.</p>
                    <p class="small text-muted mb-0">&copy;Ssonyan</p>
                </div>


            </div>
        </div>
    </footer>
</body>

</html>